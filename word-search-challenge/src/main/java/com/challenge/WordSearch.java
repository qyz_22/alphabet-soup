package com.challenge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordSearch {

	private char[][] wordGrid;
	private List<String> words = new ArrayList<String>();
	int gridRows = 0;
	int gridCols = 0;
	String endPoint = "";

	public WordSearch(){
		
	}
	
	//find all words from the grid
	public void findWordsPositions() {
		readFile(getFileName());
		
		for (String word: words) {
			String result = "";
			String tmpWord = word.replace(" ", "");
			if (tmpWord.length() <= Math.max(gridRows, gridCols)) {
				result = findWord(tmpWord);
			}
			System.out.println(word + " " + result);
		}
		
	}
	
	//search the word in the grid
	private String findWord(String word) {
		String result = "";
		endPoint = "";
		
		for (int i= 0; i< gridRows; i++) {
			for (int j=0; j< gridCols; j++) {
				if(wordGrid[i][j]==word.charAt(0) && (dfsRightDown(wordGrid,i,j,0,word) || dfsDiagonal(wordGrid,i,j,0,word))) {
					result =  i + ":" + j + " " + endPoint;
				}
			}
		}
		return result;
	}
	
	// check vertical, horizontal forwards or backwards
	private boolean dfsRightDown(char[][] grid, int i, int j, int checkPos, String word) {
		
		if(checkPos == word.length()) {
			return true;
		}
		
		if(i<0 || i>=gridRows || j<0 || j>=gridCols || wordGrid[i][j]!=word.charAt(checkPos)) {
			return false;
		}
		
		boolean found;
		int nextPos = checkPos +1;
		//check to right, down or backwards
		found = dfsRightDown(wordGrid, i, j+1, nextPos, word) 
				|| dfsRightDown(wordGrid, i+1, j, nextPos, word)
				|| dfsRightDown(wordGrid, i, j-1, nextPos, word)
				|| dfsRightDown(wordGrid, i-1, j, nextPos, word);
		
		
		if(found && nextPos == word.length()) {
			//get end position
			endPoint =  i + ":" + j;
		}
		
		return found;
	}
	
	//check diagonal forwards or backwards
	private boolean dfsDiagonal(char[][] grid, int i, int j, int checkPos, String word) {
		
		if(checkPos == word.length()) {
			return true;
		}
		
		if(i<0 || i>=gridRows || j<0 || j>=gridCols || wordGrid[i][j]!=word.charAt(checkPos)) {
			return false;
		}
		
		boolean found;
		int nextPos = checkPos +1;
		//diagonal or backwards
		found = dfsDiagonal(wordGrid, i+1, j+1, nextPos, word)
				|| dfsDiagonal(wordGrid, i-1, j-1, nextPos, word)
				|| dfsDiagonal(wordGrid, i+1, j-1, nextPos, word)
				|| dfsDiagonal(wordGrid, i-1, j+1, nextPos, word);
		
		
		if(found && nextPos == word.length()) {
			//get end position
			endPoint =  i + ":" + j;
		}
		
		return found;
	}

	//read file and process
	private void readFile(String fileName) {
		try{
			File file = new File (fileName);
			FileReader fileReader = new FileReader (file);
			BufferedReader bufferedRead = new BufferedReader(fileReader);

			int fileLine = 0;
			String readLine = "";
			
			while ((readLine = bufferedRead.readLine()) != null) {
				fileLine++;
				processFileLine(fileLine, readLine);
			}
			
			fileReader.close();
			bufferedRead.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//get file name from input
	private String getFileName() {
		String result = "";
		System.out.println("Please enter a word search file:");
		BufferedReader bufferRead = new BufferedReader( new InputStreamReader( System.in ) );
		try {
			result = bufferRead.readLine();
			bufferRead.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 return result;
	}
	
	//Process each file line;
	private void processFileLine(int fileLine, String readLine) {

		if (fileLine == 1) { //first line -read grid size: row x column
			String[] wordSize = readLine.split("x");
			gridRows = Integer.parseInt(wordSize[0]);
			gridCols = Integer.parseInt(wordSize[1]);
			wordGrid = new char[gridRows][gridCols];
		} 
		else if (fileLine<=gridRows+1) { //word grid from line 2 to line <gridRows+1> 
			String rowChars = readLine.replace(" ", "");
			wordGrid[fileLine-2] = Arrays.copyOf(rowChars.toCharArray(),gridCols);		
		}
		else { //read words to be found from line <gridRows+2> to end
			words.add(readLine);
		}
	}
	
	public static void main(String[] args) {
		WordSearch wordSearch = new WordSearch();
		wordSearch.findWordsPositions();
	}

}
